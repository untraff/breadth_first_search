MAP_FILE_NAME = 'example_data.txt'


class Node:

    def __init__(self, coordinate: tuple, parent_node=None):
        self.parent_node = parent_node
        self.x, self.y = coordinate

    def __str__(self):
        return f'Node({self.x}, {self.y})'

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return (getattr(self, 'x', None), getattr(self, 'y', None)) == \
               (getattr(other, 'x', None), getattr(other, 'y', None))

    def __hash__(self):
        return hash((self.x, self.y))


def create_tree(_map: list) -> dict:
    """Создаем словарь для быстрого поиска, ключ - tuple координат, значение - 0"""
    # TODO: Подумать, может стоит собрать структуру с вложенностями.
    tree = {}
    for y in range(0, len(_map)):
        for x in range(0, len(_map[y])):
            if _map[y][x] == 1:  # фильтруем не ноды (1)
                continue
            tree[(x, y)] = _map[y][x]
    return tree


def get_neighbours(parent_node: Node, data) -> list:
    """Получаем список всех соседних узлов"""
    neighbours = []
    neighbour_choises = [(0, 1), (1, 0), (0, -1), (-1, 0)]
    for v in neighbour_choises:
        neighbour_x = (parent_node.x + v[0])
        neighbour_y = (parent_node.y + v[1])
        neighbour_val = data.get((neighbour_x, neighbour_y))
        if neighbour_val == 0:  # отсеиваем единицы (стенки)
            neighbours.append(Node((neighbour_x, neighbour_y), parent_node))
    return neighbours


def get_map_data(filename: str) -> list:
    """Получаем карту лабиринта из файла, формируем массив вида
    [[],[],[],]
    """
    with open(filename, 'r') as f:
        map_valid_data = []
        for line in f.readlines():
            validated_line = line.rstrip()
            map_valid_data.append([int(dot) for dot in validated_line])
        return map_valid_data


def main():

    _map = get_map_data(MAP_FILE_NAME)
    tree = create_tree(_map)

    start_node = Node((0, 0), None)
    end_node = Node((len(_map)-1, len(_map[0])-1))

    pass_nodes = {}
    queue = [start_node]  # start
    result_way = None

    while queue:
        node = queue.pop(0)
        pass_nodes.setdefault(node, 0)
        neighbours = get_neighbours(node, tree)
        for neighbour in neighbours:
            if neighbour in pass_nodes:
                continue
            if neighbour == end_node:  # искомый узел
                parent_node = neighbour.parent_node
                way = [end_node, parent_node]
                while parent_node.parent_node:
                    parent_node = parent_node.parent_node
                    way.append(parent_node)
                result_way = way[::-1]
                break
            if neighbour != node.parent_node:
                queue.append(neighbour)
    print(*result_way)


if __name__ == '__main__':
    main()
